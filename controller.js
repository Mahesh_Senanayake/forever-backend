const axios = require("axios");

exports.foreverDependencies = async (req, res) => {
  await axios
    .get("https://registry.npmjs.org/forever/latest")
    .then((resp) => {
      let object = resp.data.dependencies;
      let dataArray = [];
      for (var i in object) {
        dataArray.push(i, object[i]);
      }
      console.log(dataArray);
      res.status(200).json(dataArray);
    })
    .catch((err) => {
      res.status(400).json({
        err: err,
      });
    });
};
